/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.storeproject.poc;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Xenon
 */
public class TestConnection {
    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Data connnection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error: JDBC is not exist");
        } catch (SQLException ex) {
            System.out.println("Error: Database cannot connect");
        }
        
        try {
            if(conn != null) {
                conn.close();   
            }
        } catch (SQLException ex) {
            System.out.println("Error: Cannot close database");
        }
    }

}